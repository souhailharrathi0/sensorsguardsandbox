import os
import pathlib

from pydantic_settings import SettingsConfigDict, BaseSettings

env_file = os.path.join(pathlib.Path(__file__).parent.parent.parent, '.env')


class StreamerSettings(BaseSettings):
    BROKER_HOSTS: list[str]
    SENSORS_TOPIC: str
    WATER_CONTROL_TOPIC: str
    LOG_LEVEL: str
    LOG_FILENAME: str = ""
    model_config = SettingsConfigDict(env_file=env_file, env_prefix="STREAMER_", case_sensitive=True)


class Settings(BaseSettings):
    STREAMER: StreamerSettings = StreamerSettings()


settings = Settings()
